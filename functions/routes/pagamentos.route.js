const express = require('express');
const router = express.Router();

const pagamentosController = require('../controllers/pagamentos.controller');

router.get('/pagamentos', (req, res) => pagamentosController.lista(req, res));
router.get('/pagamentos/:id', (req, res) => pagamentosController.listaById(req, res));
router.post('/pagamentos', (req, res) => pagamentosController.criar(req, res));
router.put('/pagamentos/:id', (req, res) => pagamentosController.alterar(req, res));
router.delete('/pagamentos/:id', (req, res) => pagamentosController.remover(req, res));

module.exports = router;
