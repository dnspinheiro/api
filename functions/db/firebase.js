const firebase_admin = require('firebase-admin');

try {
    firebase_admin.initializeApp();
} catch (e) {
    console.log('e firebase', e);
}
module.exports = firebase_admin;

// var firebase = require("firebase-admin");

// var serviceAccount = require("./serviceAccountKey.json");

// firebase.initializeApp({
//     credential: firebase.credential.cert(serviceAccount)
// });

// module.exports = firebase;