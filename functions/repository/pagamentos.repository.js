'use strict'
// PagamentosRepository.js

const firebase = require('../db/firebase');
// const firebase = require('firebase-admin');
const db = firebase.firestore();

const lista = async () => {
    var associadoRef = db.collection('Pagamentos/');
    return associadoRef.get().then(snapshot => {
        var array = [];
        snapshot.forEach((doc) => {
            var obj = doc.data();
            obj.id = doc.id;
            array.push(obj);
        });
        return array;
    });
}

const listaById = async (id) => {
    return db.collection('Pagamentos/').doc(id).get().docs;
}

const criar = async (pagamento) => {
    return db.collection('Pagamentos/').add(pagamento);
}

const alterar = (id, pagamento) => {
    return db.collection('Pagamentos/').doc(id).update(pagamento);
};

const remover = (id) => {
    return db.collection('Pagamentos/').doc(id).delete();
};

module.exports = {
    lista,
    listaById,
    criar,
    alterar,
    remover
};