const functions = require("firebase-functions");
const express = require('express');
const cors = require('cors');
var pagamento = require('./routes/pagamentos.route');

var app = express();
app.use(cors());
app.set('view engine', 'html');
// app.set('views', path.join(__dirname, 'views'));

app.use(pagamento);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error', { err: err });
});

// The Firebase Admin SDK to access Firestore.
// const admin = require('firebase-admin');
// admin.initializeApp();

module.exports.bradesco_api = functions.https.onRequest(app);