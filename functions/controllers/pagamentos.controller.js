'use strict'
// PagamentosController.js
const repository = require('../repository/pagamentos.repository');

const listaById = async (req, res) => {

    repository.listaById(req.params.id_associado)
        .then(array => {
            if (array.length > 0) {
                return res.send(array);
            } else {
                return res.status(500).send(array);
            }
        })
        .catch((err) => {
            return response.error(res, 'DefaultException', 'Erro ao carregar produtos.');
        });
};

const lista = async (req, res) => {

    repository.lista()
        .then(array => {
            return res.send(array);
        })
        .catch((err) => {
            return res.status(500).send(err);
        });
};

const criar = async (req, res) => {
    repository.criar(req.body)
        .then(retorno => {
            return res.send(retorno);
        })
        .catch((err) => {
            return res.status(500).send(err);
        });
}
const alterar = async (req, res) => {
    repository.alterar(req.params.id, req.body)
        .then(retorno => {
            return res.send(retorno);
        })
        .catch((err) => {
            return res.status(500).send(err);
        });
}

const remover = async (req, res) => {
    repository.remover(req.params.id)
        .then(retorno => {
            return res.send(retorno);
        })
        .catch((err) => {
            return res.status(500).send(err);
        });
}

module.exports = {
    lista,
    listaById,
    criar,
    alterar,
    remover
};
